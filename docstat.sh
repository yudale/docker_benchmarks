#!/bin/bash
name=${1-portfolio_sync_container}
exec docker stats --format "table {{.CPUPerc}}\t{{.MemUsage}}\t{{.MemPerc}}\t{{.NetIO}}\t{{.BlockIO}}\t{{.PIDs}}" $name
