#!/usr/bin/env python3
#from __future__ import print_function
import sys
import re
from pathlib import Path
from dataclasses import dataclass, astuple

log = 'tmp.log'
stats = 'tmp.stats'
opt = 'tmp.csv'

@dataclass
class Bench:
    time_taken: float
    reqs_per_sec: float
    time_per_req: float
    time_per_req_concurrent: float
    time_longest: float
    cpu_percent: float = 0
    memory_mb: float = 0
    network_out_mb: float = 0
    proc: int = 0

    @classmethod
    def from_str(cls, *args):
        return cls(*map(float, args))

def get_log(filename):
    text = Path(__file__).with_name(filename).read_text()
    res = Bench.from_str(
        re.search('Time taken for tests:\s+(\S+)', text)[1],
        re.search('Requests per second:\s+(\S+)', text)[1],
        re.search('Time per request:\s+(\S+) \[ms\] \(mean\)', text)[1],
        re.search('Time per request:\s+(\S+) \[ms\] \(mean,', text)[1],
        re.search('\nTotal:\s.*\s(\S+)\n?$', text)[1],
        #re.search('\s(\S+) \(longest request', text)[1],
        )
    return res

def get_stats(filename):
    def pow(s, p):
        if p == 'M': return float(s)
        if p == 'G': return float(s) * 1024
        if p == 'k': return float(s) / 1024
        if p == 'T': return float(s) * 1024 * 1024

    cpu, mem, net, proc = [], [], [], []
    path = Path(__file__).with_name(filename)
    for line in path.open():
        # 210.17%             194.4MiB / 15.46GiB   836kB / 3.87MB      20
        m = re.search('^(\d\S+)%\s+(\d\S+)([kMGT])iB\s+/\s+\S+\s+\S+\s+/\s+(\d\S*)([kMGT])B\s+(\d+)', line)
        if m:
            # print('+', line, end='', file=sys.stderr)
            cpu.append(float(m[1]))
            mem.append(pow(m[2], m[3]))
            net.append(pow(m[4], m[5]))
            proc.append(int(m[6]))
    return max(cpu), max(mem), max(net), max(proc)

def main():
    bench = get_log(log)
    bench.cpu_percent, bench.memory_mb, bench.network_out_mb, bench.proc = get_stats(stats)
    print(','.join(map(str, astuple(bench))))

if __name__ == '__main__':
    main()
