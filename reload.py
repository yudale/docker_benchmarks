#!/usr/bin/env python3
import sys, os, signal

DOC = '''\
[uwsgi]
#socket = /tmp/uwsgi.sock
#chown-socket = nginx:nginx
#chmod-socket = 664
module = api
callable = app
pythonpath = /app
uid = bondit
master = true
#log-master = true
#memory-report = true
#pidfile = /tmp/uwsgi.pid
thunder-lock = true
die-on-term = true
harakiri = 60  # kill if response takes > 5min
harakiri-verbose = true
listen = 128
max-requests = 0
#cheaper-algo = spare
#cheaper-initial = 8    # starts with minimal workers
#cheaper-idle = 60      # cheap one worker per minute while idle
cheaper-step = 4       # spawn at most 4 workers at once
workers = {w}           # maximum number of workers
cheaper = {c}            # tries to keep 4 idle workers
threads = {t}
enable-threads = true
'''

def main(w=16, c=8, t=1):
    doc = DOC.format_map(locals())
    open('/app/uwsgi.ini', 'w').write(doc)
    #os.system('uwsgi --reload /tmp/uwsgi.pid')
    # signal the supervisord (pid 1) to restart the uwsgi
    os.kill(1, signal.SIGHUP)

if __name__ == '__main__':
    main(*sys.argv[1:])
