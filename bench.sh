#!/bin/bash
test "$1" = -t && { shift; testing=1; }
concurrent=${1-2}
container=${2:?not set}
#cheaper=${2-8}
port=5005
port=80
path=http://0.0.0.0:$port/api/v3/portfolios/11676
method=GET
repeat=5000
repeat=150
warmup=50
name=$container
#app=portfolio_sync name=${app}_container
log=tmp.log
stats=tmp.stats

token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NTU4MzYwNjEsIm9yZ19pZCI6InVzLWVhc3QtMV84MzRidzU4dXciLCJjbGllbnRfaWQiOiI3ampubGg4YmRrODhjam1kaHE4NWxwam10aiIsInNjb3BlcyI6WyJuYWl2ZSIsImNoYW5nZV9wYXNzd29yZCJdLCJyb2xlIjoidXNlciIsImN0IjpudWxsLCJ1c2VyX2lkIjoiMmM0N2M5ODctODFlZi00MTI4LTg5MTctYWFhZDE1ZDg0YTBiIn0.TRPXEMQCO0wAZNZTZ4006o-FPyaS8Wt41L6h5qMjuGI

head="-H accept:application/json" 
#head="$head -T application/json"
set -- $head -H "Authorization:Bearer $token"

if "$testing"; then
    exec curl -X $method "$@" $path
fi

# warm-up
#ab -q -m $method
#ab -d -n $warmup -c $concurrent "$@" $path >& /dev/null

docker stats --format "table {{.CPUPerc}}\t{{.MemUsage}}\t{{.NetIO}}\t{{.PIDs}}" $name > $stats &
pid=$!
trap "kill $pid" 0
sleep 1

#ab -q -m $method
ab -d -n $repeat -c $concurrent "$@" $path | tee $log

sleep 1
