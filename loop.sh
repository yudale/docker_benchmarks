#!/bin/bash
container=${1-$(docker ps -qlf status=running)}
container=${container:?not set}
for w in 20; do
    for c in 4 8 16; do
        for t in 1 2; do
            for p in 1 10 50 100; do
                #w=16 c=4 t=2 p=100
                echo -e \\n============= $w $c $t $p - $container
                time ./run.sh $w $c $t $p $container
            done
        done
    done
done
