#!/bin/bash
workers=${1-16}
cheaper=${2-8}
threads=${3-1}
concurrent=${4-10}
container=${5-$container}
container=${container:?env variable not set}
out=stats.csv

header="workers,cheaper,threads,concurrency,time_taken_s,reqs_per_sec,time_per_req_ms,conc_time_per_req_ms,longest_req_ms,cpu_percent,memory_mb,network_out_mb,processes"
test -s $out || echo $header >> $out

#docker exec -it $container uwsgi --show-config
docker exec -it $container python3 /app/reload.py $workers $cheaper $threads
sleep 4

./bench.sh $concurrent $container
sleep 2

st=$(./parse_stats.py)
res=$workers,$cheaper,$threads,$concurrent,$st
echo + $res
echo $res >> $out
